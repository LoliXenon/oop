/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programming.w2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nate
 */
public class CashRegisterTest
    {
    
    public CashRegisterTest()
        {
        }
    
    @BeforeClass
    public static void setUpClass()
        {
        }
    
    @AfterClass
    public static void tearDownClass()
        {
        }
    
    @Before
    public void setUp()
        {
        }
    
    @After
    public void tearDown()
        {
        }

    /**
     * Test of addItem method, of class CashRegister.
     */
    @Test
    public void testAddItem()
        {
        System.out.println("addItem");
        double price = 10.0;
        CashRegister instance = new CashRegister();
        instance.addItem(price);
        assertEquals(price, instance.(), 0)
        assertEquals(10.00, instance.getTotal,0)
        }

    /**
     * Test of getTotal method, of class CashRegister.
     */
    @Test
    public void testGetTotal()
        {
        System.out.println("getTotal");
        CashRegister instance = new CashRegister();
        instance.addItem(10);
        instance.addItem(20);
        instance.addItem(30);
        double expResult = 60.0;
        double result = instance.getTotal();
        assertEquals(expResult, result, 0.0);
        
        }

    /**
     * Test of getCount method, of class CashRegister.
     */
    @Test
    public void testGetCount()
        {
        System.out.println("getCount");
        CashRegister instance = new CashRegister();
        instance.addItem(1);
        instance.addItem(2);
        int expResult = 2;
        int result = instance.getCount();
        assertEquals(expResult, result);
        }

    /**
     * Test of clear method, of class CashRegister.
     */
    @Test
    public void testClear()
        {
        System.out.println("clear");
        CashRegister instance = new CashRegister();
        instance.addItem(10.0);
        instance.addItem(20.0);
        instance.addItem(50.0);
        instance.clear();
        assertEquals(0.0, instance.getTotal(), 0);
        assertEquals(0, instance.getCount(), 0);
        }
    
    }
