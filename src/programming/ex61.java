/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programming;

import java.util.Random;

/**
 *
 * @author Nate
 */
public class ex61
    {

    public static void main(String[] args)
        {
        int[] array;
        array = new int[10];
        generateArray(array);
        System.out.println("\n");
        printIntArray(array);
        System.out.println("\n");
        evenIndex(array);
        System.out.println("\n");
        evenPrint(array);
        System.out.println("\n");
        reversePrint(array);
        System.out.println("\n");
        firstLast(array);
        }

    public static void generateArray(int[] array)
        {
        Random rand = new Random();
        for (int i = 0; i < array.length; i++)
            {
            int n = rand.nextInt(100) + 1;
            array[i] = n;
            }
        System.out.println("Array Generated");
        }
    
    public static void printIntArray(int[] test)
        {
        System.out.println("Printing Array: ");

        for (int i = 0; i < test.length; i++)
            {
            System.out.print(test[i] + " ");
            }
        }

    public static void evenIndex(int[] test)
        {
        System.out.println("Printing the Even Indexes: ");
        for (int i = 0; i < test.length; i++)
            {
            if (i % 2 == 0)
                {
                System.out.print(test[i] + " ");
                }
            }
        }
    
    public static void evenPrint(int[] test)
        {
        System.out.println("Printing only even numbers: ");
        for (int i = 0; i < test.length; i++)
            {
            if (test[i] % 2 == 0)
                {
                System.out.print(test[i] + " ");
                }
            }
        }

    public static void reversePrint(int[] test)
        {
        System.out.println("Printing in Reverse: ");
        for (int i = test.length - 1; i >= 0; i--)
            {
            System.out.print(test[i] + " ");
            }
        }

    public static void firstLast(int[] test)
        {
        System.out.println("Printing the First and last numbers: ");
        System.out.print(test[0] + " ");
        System.out.println(test[test.length - 1] + " ");
        }
    }
