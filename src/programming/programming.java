/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programming;

/**
 *
 * @author Nate
 */
public class programming
    {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        {

        int a = 20;
        int b = 30;
        swapInts(a, b);
        System.out.println(a + b);

        int[] test =
            {
            1, 2, 3, 4, 5
            };
        printIntArray(test);
        swapArray(test);
        System.out.println("\n");
        printIntArray(test);

         
        
        }

    public static void printIntArray(int[] test)
        {

        for (int i = 0; i < test.length; i++)
            {
            System.out.println(test[i]);
            }
        }

    public static void swapInts(int a, int b)
        {
        int temp = a;
        a = b;
        b = temp;

        }

    public static void swapArray(int[] test)
        {
        int temp = test[0];
        test[0] = test[test.length - 1];
        test[test.length - 1] = temp;
        }

    /*public static int findLargestInt(int[] test)
        {

        int largest = test[0];
        
        for (int i = 0; i < test.length; i++)
            {
            if (test[i] > largest)
                {
                largest = test[i];
                }
            }

        return;
        }*/
    }
