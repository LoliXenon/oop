/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programming;

/**
 *
 * @author Nate
 */
public class ex62
    {

    public static void main(String[] args)
        {
        int[] array =
            {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 0
            };
        System.out.println("\n");
        printIntArray(array);
        System.out.println("\n");
        //swapLastAndFirst(array);
        System.out.println("\n");
        //shiftElementsRight(array);
        System.out.println("\n");
        //replaceEvenElements(array);
        System.out.println("\n");
        replaceWithLarger(array);
        System.out.println("\n");
        removeMiddleEntries(array);

        }

    public static void swapLastAndFirst(int[] test)
        {
        int swap = test[0];
        test[0] = test[test.length - 1];
        test[test.length - 1] = swap;
        for (int i = 0; i < test.length; i++)
            {
            System.out.print(test[i] + " ");
            }
        }

    public static void shiftElementsRight(int[] test)
        {

        int last = test[test.length - 1];
        for (int i = test.length - 2; i >= 0; i--)
            {
            test[i + 1] = test[i];
            }
        test[0] = last;
        for (int p = 0; p < test.length; p++)
            {
            System.out.print(test[p] + " ");
            }
        }

    public static void replaceEvenElements(int[] test)
        {
        for (int i = 0; i < test.length; i++)
            {
            if (test[i] % 2 == 0)
                {
                test[i] = 0;
                }

            System.out.print(test[i] + " ");
            }
        }

    public static void replaceWithLarger(int[] test)
        {
        System.out.print(test[0] + " ");
        int currentNumber;
        for (int i = 1; i < test.length - 1; i++)
            {
            currentNumber = test[i];
            if (test[i] < test[i - 1])
                {
                test[i] = test[i - 1];
                }
            if (test[i] < test[i + 1])
                {
                test[i] = test[i + 1];
                }
            test[i + 1] = currentNumber;
            System.out.print(test[i] + " ");
            }
        System.out.print(test[test.length - 1]);
        }

    public static void removeMiddleEntries(int[] test)
        {
        if (test.length % 2 == 0)
            {
            test[(test.length / 2)] = 0;
            test[(test.length / 2) - 1] = 0;
            } else
            {
            test[(test.length / 2)] = 0;
            }
        for (int i = 0; i < test.length; i++)
            {
            System.out.print(test[i] + " ");
            }
        }
    
    public static void orderEvensFirst(int[] test)
        {
        
        }

    public static void printIntArray(int[] array)
        {
        System.out.println("Printing Array: ");

        for (int i = 0; i < array.length; i++)
            {
            System.out.print(array[i] + " ");
            }
            System.out.println("\n");
        }

    }
