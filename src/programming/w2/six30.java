/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programming.w2;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Nate
 */
public class six30
    {

    public static void main(String[] args)
        {
        ArrayList<Double> salesAmount = new ArrayList<Double>();
        ArrayList<String> customers = new ArrayList<String>();
        registerDetails(salesAmount, customers);
        System.out.println(customers);
        System.out.println(salesAmount);
        System.out.println(nameOfBestCustomer(salesAmount, customers));
        }

    public static void registerDetails(ArrayList<Double> sale, ArrayList<String> customers)
        {
        Scanner sc = new Scanner(System.in);
        Scanner num = new Scanner(System.in);
        System.out.println("Please enter all customer names. Entering 0 will finish.");

        String name = sc.next();
        while (!name.equals("0"))
            {
            customers.add(name);
            name = sc.next();
            }

        System.out.println("Please enter all customer sales totals.");
        double sales = num.nextDouble();
        for (int i = 0; i < customers.size(); i++)
            {
                sale.add(sales);
                sales = num.nextDouble();
            }

        }

    public static String nameOfBestCustomer(ArrayList<Double> sales, ArrayList<String> customers)
        {
        double highestSpend = findHighestSpend(sales);
        ArrayList<Integer> bigSpenderIndex = findBigSpenders(sales, highestSpend);
        String bigSpenders = "";
        for (int i = 0; i < bigSpenderIndex.size(); i++)
            {
            bigSpenders += customers.get(bigSpenderIndex.get(i)) + " ";
            }
        return bigSpenders;
        }

    public static double findHighestSpend(ArrayList<Double> sales)
        {
        double high = 0;
        for (double sale : sales)
            {
            if (sale > high)
                {
                high = sale;
                }
            }
        return high;
        }

    public static ArrayList<Integer> findBigSpenders(ArrayList<Double> sales, double highestSpend)
        {
        ArrayList<Integer> bigSpenderIndex = new ArrayList<>();
        for (int i = 0; i < sales.size(); i++)
            {
            if (sales.get(i) == highestSpend)
                {
                bigSpenderIndex.add(i);
                }
            }
        return bigSpenderIndex;
        }

    }
