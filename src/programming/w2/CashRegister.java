/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programming.w2;

/**
 *
 * @author Nate
 */
public class CashRegister
    {
    /**
     *adds an item to the cash register
     *@param price of the item
    */
    
    private double registerTotal;
    private int itemCount;
    /**
     * 
     * Constructor 
     */
    public void CashRegister()
        {
        registerTotal = 0;
        itemCount = 0;
        
        }
    public void addItem(double price)
        {
        if (price>0 ){
       itemCount++;
       registerTotal += price;
        }
        }
    /**
        Get the price of all items in the sale
        @return the total price
    */
    
    public double getTotal()
        {
        return registerTotal;
        }
    
    /**Gets number of items in sale
    @return the item count
    */
    
    public int getCount()
        {
        return itemCount;
        }
    
    /*
    Clear the item count
    
    */
    public void clear()
        {
        itemCount=0;
        registerTotal = 0;
        }
    }
