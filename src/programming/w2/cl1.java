/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package programming.w2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import static programming.ex62.printIntArray;
/**
 *
 * @author Nate
 */
public class cl1
    {
    enum Options{CREATE, READ, UPDATE, DELETE;}
    
    public static void main(String[] args)
        {
        //testEnums();
        int[] testArray = {1,2,3,4,5};
        printIntArray(testArray);
        copyArray(testArray);
        printIntArray(testArray);
        printXOs();
        }
    
    public static void testEnums()
        {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        Options option = Options.values()[input];
        switch(option)
            {
            case CREATE:
                System.out.println("Want to create");
                break;
            case READ:
                System.out.println("Want to read");
            }
        }
    
    public static void copyArray(int[] array)
        {
        //Shallow copy, writes to original array
//        int[] arrayCopy = array;
//        arrayCopy[0] = 100000;
        
        //Deep copy, copies array properly
        int[] arrayCopy = Arrays.copyOf(array, array.length);
        }
    
    public static void printXOs()
        {
        char[][] table = {{'X', 'O', 'O'}, {'O', 'O', 'X'}, {'X', 'X', 'X'}};
        
        for (int i = 0; i< table.length; i++)
            {
            for (int j = 0; j < table[0].length;j++)
                {
                    System.out.print(table[i][j] + " ");
                }
                System.out.println("");
            }
        }
//    public static void arrayListThings()
//        {
//        ArrayList<String> = new ArrayList<String>();
//        
//        
//        }
    }
